const User = require("../models/user");
const Orders = require("../models/orders");
const Product = require("../models/product");

module.exports.checkout = (userData, cart) => {
	return Orders.findOne({userId: userData.userId}).then(user => {
		if(user === null){
			return false;
		} else {
			let newOrder = new Order {
					totalAmount: cart.totalAmount
			}
		}
			return orders.save().then((order, error) =>{
				if(error){
					return false;
				} else {
					const currentOrder = order.products[order.products.length-1];
					
					currentOrder.products.forEach(product => {
						Product.findById(product.productId).then(foundProduct => {
							foundProduct.products.push({orderId: currentOrder._id})

							foundProduct.save()
						})
					});

					return true;
				}

		})
	})
};

module.exports.getOrders = (userId) => {
	return Orders.findOne({userId: userId}).then(user => {
		if(user === null){
			return false;
		} else {
			return user;
		}
	})
};
const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderController = require("./orderController");

router.post("/checkout", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === true) {
		res.send(false);
	} else {
		const userData = auth.decode(req.headers.authorization);
		orderController.checkout(userId, req.body).then(resultFromController => res.send(resultFromController));
	}
})
router.get("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true) {
		res.send(false);
	} else {
		orderController.getMyOrders(userData.id).then(resultFromController => res.send(resultFromController));
	}
})

module.exports = router;
const mongoose = require("mongoose");
const User = require("./user.js")

const orderSchema = new mongoose.Schema({ 
	_id: Schema.Types.ObjectId,
	userId:{
		type: Schema.Types.ObjectId, ref: 'User'
	},
	products : [
		{
			productId : {
				type: String,
				required : [true, "Product ID is required"]
			},
			quantity : {
				type: Number,
				required : [true, "Product quantity is required"]
			}
		}
	],
	totalAmount : {
		type: Number,
		required : [true, "Total amount is required"]
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	}
})

module.exports = mongoose.model("Orders", orderSchema);